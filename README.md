# Script setup Mac OSX

This script aims to speed up installation of a MacOs system. It sets preferences and install applications so you don't have to.

## Usage

```bash
git clone https://thomas_g@bitbucket.org/thomas_g/script_setup_macos.git
cd script_setup_macos
bin/setup.sh
```

## References

Also check [Laptop from Thoughtbot] and [Strap from Coraline Ada Emke].

[Laptop from Thoughtbot]: https://github.com/thoughtbot/laptop
[Strap from Coraline Ada Emke]: https://github.com/CoralineAda/strap

#!/bin/bash
#/ Usage: bin/setup.sh [--debug]

set -e

if [ "$1" = "--debug" ]; then
  set -x
else
  Q="-q"
fi

abort() { echo "!!! $*" >&2; exit 1; }
log()   { echo "--> $*"; }
logn()  { printf -- "--> $* "; }
logk()  { echo "OK"; }

log "Enter the name you will be using for git:"
read GIT_NAME
log "Enter the email you will be using for git:"
read GIT_EMAIL

[ "$USER" = "root" ] && abort "Run Strap as yourself, not root."
groups | grep $Q admin || abort "Add $USER to the admin group."

# Initialise sudo now to save prompting later.
log "Enter your password (for sudo access):"
sudo echo 'sudo initialised'
logk

# Check and enable full-disk encryption.
logn "Checking full-disk encryption status:"
if fdesetup status | grep $Q -E "FileVault is (On|Off, but will be enabled after the next restart)."; then
  logk
else
  echo
  logn "Enabling full-disk encryption on next reboot:"
  sudo fdesetup enable -user "$USER" \
    | tee ~/Desktop/"FileVault Recovery Key.txt"
  logk
fi

# Setup Git configuration.
logn "Configuring Git:"
if [ -n "$GIT_NAME" ] && ! git config user.name >/dev/null; then
  git config --global user.name "$GIT_NAME"
fi

if [ -n "$GIT_EMAIL" ] && ! git config user.email >/dev/null; then
  git config --global user.email "$GIT_EMAIL"
fi

# Install Homebrew.
if test ! $(which brew); then
  logn "Installing Homebrew:"
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi
logk

# Update Homebrew.
export PATH="$usr/local/bin:$PATH"
log "Updating Homebrew:"
brew update
logk

# Install Homebrew Cask and Services tap.
log "Installing Homebrew taps and extensions:"
brew bundle --file=- <<EOF
tap 'caskroom/cask'
tap 'homebrew/services'
EOF
logk

# Check and install any remaining software updates.
logn "Checking for software updates:"
if softwareupdate -l 2>&1 | grep $Q "No new software available."; then
  logk
else
  echo
  log "Installing software updates:"
  sudo softwareupdate --install --all
  logk
fi

# Install from local Brewfile.
log "Installing from Brewfile"
brew bundle
logk

# Install oh-my-zsh.
log "Installing oh-my-zsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/loket/oh-my-zsh/feature/batch-mode/tools/install.sh)" -s --batch || {
  echo "Could not install Oh My Zsh" >/dev/stderr
  exit 1
}
logk

# Load .zshrc config
read -p "Do you wish to use custom .zshrc (y/n - enter to skip)?" answer
if [[ "$answer" =~ [yY] ]]; then
  log "Personalizing .zshrc"
  modules_pattern="  git"
  modules_string="  git
  rails
  ruby
  docker
  zsh-autosuggestions
  osx
  autojump"

  perl -0777 -i -pe "s/$modules_pattern/$modules_string/g" ~/.zshrc
  cat <<-EOT >> ~/.zshrc
eval "\$(direnv hook zsh)"
[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh

alias zshconfig="subl ~/.zshrc"
alias ohmyzsh="subl ~/.oh-my-zsh"
alias hostsconfig="subl /etc/hosts"
alias sshconfig="subl ~/.ssh/known_hosts"
EOT
  cat <<-EOT >> ~/.gitconfig
[alias]
  co = checkout
  ci = commit
  st = status
  br = branch
  hist = log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short
  type = cat-file -t
  dump = cat-file -p
EOT
logk
fi

# Dock settings.
logn "Configuring dock settings:"
defaults write com.apple.dock tilesize -int 64
defaults write com.apple.dock largesize -int 92
defaults write com.apple.dock autohide -bool true
defaults write com.apple.dock magnification -bool true
logk

# Trackpad settings.
logn "Configuring trackpad settings:"
defaults write com.apple.AppleMultitouchTrackpad TrackpadRightClick -bool true
defaults write com.apple.AppleMultitouchTrackpad Clicking -bool true
defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false
logk

# Finder settings.
logn "Configuring finder settings:"
defaults write com.apple.Finder AppleShowAllFiles true
defaults write com.apple.DiskUtility DUShowEveryPartition -bool true
defaults write NSGlobalDomain AppleShowAllExtensions -bool true
defaults write com.apple.finder ShowPathbar -bool true
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
logk

# Hot corner settings.
logn "Configuring finder settings:"
defaults write com.apple.dock wvous-tr-corner -int 5
defaults write com.apple.dock wvous-tr-modifier -int 0
defaults write com.apple.dock wvous-br-corner -int 4
defaults write com.apple.dock wvous-br-modifier -int 0
logk

# Set zoom settings using CTRL key
logn "Configuring zoom settings:"
defaults -currentHost write com.apple.universalaccess closeViewScrollWheelModifiersInt -int 262144
defaults -currentHost write com.apple.universalaccess closeViewScrollWheelToggle -bool true
logk

# Set screensaver settings
logn "Configuring screensaver settings:"
defaults -currentHost write com.apple.screensaver moduleDict -dict moduleName Aerial path $HOME/Library/Screen\ Savers/Aerial.saver type 0
if [ -n "$GIT_NAME" ] && [ -n "$GIT_EMAIL" ]; then
  sudo defaults write /Library/Preferences/com.apple.loginwindow \
    LoginwindowText \
    "Found this computer? Please contact $GIT_NAME at $GIT_EMAIL."
fi
logk

# Since com.apple.screensaver askForPassword and askForPasswordDelay do not work properly, promt user to set settings manually.
# defaults write com.apple.screensaver askForPassword -int 1
# defaults write com.apple.screensaver askForPasswordDelay -int 0
log "Please set the 'Require password' option to 'immediately'"
open /System/Library/PreferencePanes/Security.prefPane
read -p "Press enter to continue"

# Launch programs
read -p "Do you wish to launch programs (y/n - enter to skip)?" answer
if [[ "$answer" =~ [yY] ]]; then
  logn "Launch programs:"
  open /Applications
  open -a firefox
  open -a iterm
  open -a mail
  open -a docker
  open -a keepassxc
  open -a veracrypt
  open -a discord
  open -a skype
  open -a windscribe
  open -a istat\ menus
  open https://www.office.com/
  open https://forticlient.com/downloads
  open /System/Library/PreferencePanes/iCloudPref.prefPane
  logk
fi

# Ask for logout or reboot
read -p "Do you wish to logout (y/n - enter to skip)?" answer
if [[ "$answer" =~ [yY] ]]; then
  sudo pkill loginwindow
fi

read -p "Do you wish to reboot (y/n - enter to skip)?" answer
if [[ "$answer" =~ [yY] ]]; then
  sudo shutdown -r now
fi

log "Your system is now Strap'd!"
